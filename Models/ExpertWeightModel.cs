﻿namespace RAPZ_Lab6.Models
{
    public class ExpertWeightModel
    {
        public string? ExpertType { get; set; }
        public double CoefOfWeightRelational { get; set; }
    }
}
