﻿using RAPZ_Lab6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RAPZ_Lab6.ViewModels
{
    public class WeightCoefFromExpertViewModel
    {
        public List<WeightCoefFromExpertModel> WeightCoefFromExpert { get; set; } = new List<WeightCoefFromExpertModel>();

        public WeightCoefFromExpertViewModel()
        {
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Точність управління та обчислень" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Ступінь стандартності інтерфейсів" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Функціональна повнота" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Функціональна повнота" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Стійкість до помилок" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Можливість розширення" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Зручність роботи" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Простота роботи" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Відповідність чинним стандартам" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Переносимість між ПЗ" });
            WeightCoefFromExpert.Add(new WeightCoefFromExpertModel() { Critery = "Зручність навчання" });
            
        }
    }
}
