﻿using RAPZ_Lab6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RAPZ_Lab6.ViewModels
{
    public class ComplexQualityIndicatorsViewModel
    {
        public List<ComplexQualityIndicatorsModel> complexQualityIndicatorsModels { get; set; } = new();

        public ComplexQualityIndicatorsViewModel(WeightCoefFromExpertViewModel weightCoefFromExpertViewModel, List<ExpertWeightModel> expertsWeights,
            double averageRelationalWeight)
        {
            foreach (var item in weightCoefFromExpertViewModel.WeightCoefFromExpert)
            {
                ComplexQualityIndicatorsModel complexQualityIndicator = new();
                complexQualityIndicator.Critery = item.Critery;

                for (int i = 0; i < item.Weights.Count; i++)
                {
                    var mult = Math.Round(item.Weights[i] * item.Evaluations[i], 2);
                    complexQualityIndicator.Evaluations.Add(mult);
                }

                double sumProduct = 0;
                var coefOfWeightRelationalSum = expertsWeights.Sum(a => a.CoefOfWeightRelational);
                for (int i = 0; i < complexQualityIndicator.Evaluations.Count; i++)
                {
                    sumProduct += complexQualityIndicator.Evaluations[i] * expertsWeights[i].CoefOfWeightRelational;    
                }
                var firstAverageEvaluation = Math.Round(sumProduct / coefOfWeightRelationalSum, 2);
                complexQualityIndicator.AvarageEvaluations.Add(firstAverageEvaluation);
                    
                var averageWeight = item.Weights.Average();
                var secondAverageEvaluation = Math.Round(firstAverageEvaluation / averageWeight, 2);
                complexQualityIndicator.AvarageEvaluations.Add(secondAverageEvaluation);
                
                complexQualityIndicatorsModels.Add(complexQualityIndicator);
            }


        }
    }
}
